<?php

function action_arduino_dist() {
	include_spip("inc/arduino");
	$arduino = new arduino($GLOBALS["meta"]["arduino"]); // nouvel arduino avec parametres persos
	
	if ($arduino->open()) {
		
		$op  = _request("op");
		$pin = intval(_request("pin"));
		$value = intval(_request("value"));
		
		switch ($op) {
			case "analogReadAll":
				$res = $arduino->analogReadAll();
				break;
			case "analogRead":
				$res = $arduino->analogRead($pin);
				break;		
			case "digitalReadAll":
				$res = $arduino->digitalReadAll($pin);
				break;
			case "digitalRead":
				$res = $arduino->digitalRead($pin);
				break;		
			case "digitalWrite":
				$res = $arduino->digitalWrite($pin, $value);
				break;		
			case "analogWrite":
				$res = $arduino->analogWrite($pin, $value);
				break;		
		}
		
		$arduino->close();
	}
	include_spip("inc/json");
	echo json_export($res);
	exit;
}

?>
