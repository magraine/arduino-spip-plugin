<?php

function arduino_jquery_plugins($plugins){
	$plugins[] = "javascript/jquery.arduino.js";
	$plugins[] = "lib/jquery-ui-1.7.1.custom/js/jquery-ui-1.7.1.custom.min.js";
	return $plugins;
}

function arduino_insert_head($head){
	$css = "lib/jquery-ui-1.7.1.custom/css/smoothness/jquery-ui-1.7.1.custom.css";
	return $head . "\n" . '<link rel="stylesheet" href="' . $css . '" type="text/css" media="all" />';
}
?>
